import Vue from 'vue'
import Vuex from 'vuex'
import goods from './modules/goods'
import shopcart from './modules/shopcart'

Vue.use(Vuex)

export default new Vuex.Store({

  modules: {
    goods,
    shopcart
  }
  

  

  // state: {
  //   count:0,
  //   age:0
  // },
  // getters:{
  //   getterAge(state){
  //     return state.age +=10
  //   }
  // },
  // mutations: {
  //   addCount(state,obj){
  //     return state.count += obj.num;
  //   },
  //   subCount(state,obj){
  //     return state.count -= obj.num;
  //   }
  // },
  // actions: {
  //   addCountasy(context){
  //     setTimeout(()=>{
  //       context.commit('addCount',{
  //         num:2
  //       })
  //     },2000)
  //   },
  //   subCountasy(context){
  //     setTimeout(()=>{
  //       context.commit('subCount',{
  //         num:2
  //       })
  //     },2000)
  //   }
  // },
  // modules: {
  // }
})
